FROM docker.io/library/debian:bullseye as build
ENV VIRTUAL_ENV="/opt/ansible"
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN apt-get update \
    && apt-get -y --no-install-recommends install build-essential python3 python3-dev python3-pip python3-venv \
    && python3 -m venv $VIRTUAL_ENV \
    && pip --no-cache-dir install --upgrade pip \
    && pip --no-cache-dir install wheel \
    && pip --no-cache-dir install ansible ansible-lint ara

FROM docker.io/library/debian:bullseye 
LABEL name="docker-ansible"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-ansible/"
LABEL maintainer="thomas@stewarts.org.uk"

ENV VIRTUAL_ENV="/opt/ansible"
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN apt-get update \
    && apt-get -y --no-install-recommends install ca-certificates curl git python3 openssh-client \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
    
COPY --from=build $VIRTUAL_ENV $VIRTUAL_ENV

CMD ["ansible"]
