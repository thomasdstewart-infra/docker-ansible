# docker-ansible - Docker image with Ansible installed

Gitlab: [![pipeline status](https://gitlab.com/thomasdstewart-infra/docker-ansible/badges/main/pipeline.svg)](https://gitlab.com/thomasdstewart-infra/docker-ansible/-/commits/main)

Gitea: [![Build Status](https://drone.stewarts.org.uk/api/badges/thomasdstewart-infra/docker-ansible/status.svg)](https://drone.stewarts.org.uk/thomasdstewart-infra/docker-ansible)
